import uvicorn as uvicorn
from fastapi import FastAPI, responses, Request
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware

from base.exception_handlers import CustomException
from config.settings import DEBUG, CORS_ORIGIN
from file.endpoints import file_router
from game.endpoints import main_router
from users.endpoints import user_router

DESCRIPTION = '''
Спецификация АПИ [игры Сапёр](https://minesweeper-test.studiotg.ru).

Каждая игра начинается с указания размера поля **width** и **height**, а также количества мин **mines_count** на нём. 
Исходная задача не подразумевает ограничений, но для тестовой реализации остановимся на разумном ограничении входных 
параметров: ширина и высота не более 30, количество мин не более **width * height - 1** (всегда должна быть хотя бы 
одна свободная ячейка).

Далее игроку предлагается в созданной игре (идентификация игры по полученному в ответ **game_id**) делать ходы, 
указывая координаты ячейки, которую игрок хочет открыть, а именно - **row** (номер ряда, начиная с нуля) и **col** (
номер колонки, начиная с нуля).

В ответ на оба метода приходят данные о самой игре: уникальный идентификатор игры **game_id**, размер поля и 
количество мин, указанные при создании игры, а также данные о поле **field** в виде двумерного массива символов (
**height** строк, в каждой по **width** элементов), где пустые строки **" "** (пробелы) означают неоткрытые ячейки 
поля, поля с цифрами от **"0"** до **"8"** означают открытые ячейки, где цифры указывают, сколько мин расположено в 
непосредственной близости от текущей ячейки. Также возвращается параметр completed, указывающий, завершена ли текущая 
игра.

Игра заканчивается в случае, если пользователь указал на ячейку, где установлена мина (ячейки с минами при этом 
отмечены символом **"X"** - латинская заглавная "икс"), либо пользователь открыл все ячейки, не занятые минами (в 
этом случае мины отмечены **"M"** - латинская заглавная "эм"). Также при завершении игры должна появиться информация 
по всем остальным ячейкам - количество мин рядом с каждой из ячеек.

Если в процессе игры пользователь открывает ячейку, рядом с которой нет ни одной мины (то есть ячейка со значением 
**"0"**), должны "открыться" все смежные ячейки, рядом с которыми также нет ни одной мины, а также все смежные с ними 
"числовые" ячейки, рядом с которыми мины есть, с указанием их количества.

Не допускается после завершения игры делать новые ходы, а также повторно проверять уже проверенную ячейку. Эти, 
а также иные ошибочные ситуации должны возвращать ошибку с кодом **400 Bad Request** с текстовым описание ошибки в 
**error**.'''

_openapi = FastAPI.openapi


def openapi(self: FastAPI):
    """Custom openapi"""
    _openapi(self)

    for _, method_item in self.openapi_schema.get('paths').items():
        for _, param in method_item.items():
            res = param.get('responses')
            if '422' in res:
                del res['422']

    return self.openapi_schema


FastAPI.openapi = openapi

app = FastAPI(title='Minesweeper', debug=DEBUG, root_path='/api - АПИ игры Сапер', description=DESCRIPTION,
              docs_url='/api/docs', redoc_url='/api/redoc')
app.include_router(main_router, prefix='/api', tags=['Minesweeper'])
app.include_router(user_router, prefix='/api/user', tags=['User'])
app.include_router(file_router, prefix='/media', tags=['Media'])

app.add_middleware(
    CORSMiddleware,
    allow_origins=CORS_ORIGIN,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)


@app.exception_handler(CustomException)
async def custom_exception_handler(request: Request, exc: CustomException) -> responses.JSONResponse:
    """Custom exception handler"""
    if request:
        return responses.JSONResponse(status_code=400, content={'error': exc.name})


@app.exception_handler(RequestValidationError)
async def custom_exception_validation(request: Request, exc: RequestValidationError) -> responses.JSONResponse:
    """Custom exception handler"""
    if request and exc:
        error = 'ошибка разбора входных данных'
        return responses.JSONResponse(status_code=400, content={'error': error})


if __name__ == '__main__':
    uvicorn.run('main:app', host='127.0.0.1', port=8000, reload=True)
