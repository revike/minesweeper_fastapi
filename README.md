# Miner

Задача
------

* В соответствии с данной [спецификацией](https://minesweeper-test.studiotg.ru/swagger/) нужно реализовать веб-сервер с
  REST API.
  Проверить работу данного сервера можно прямо в [этой](https://minesweeper-test.studiotg.ru/) же форме, просто заменив
  URL API на свой локальный

Основные технологии
-------------------

```
* Python 3.12.0
* FastAPI 0.110.0
```

Установка и запуск
------------------

* Создаем файл .env такой же, как .env.example (меняем настройки при необходимости)

```touch .env```

* Создаем виртуальное окружение или [Запуск с помощью docker-compose](#docker)

```python3 -m venv venv```

* Активируем виртуальное окружение

```source venv/bin/activate```

* Устанавливаем зависимости

```pip install -r requirements.txt```

* Создаем БД

```alembic revision --autogenerate -m 'database create'```

* Выполняем миграции

```alembic upgrade head```

* Запуск

```uvicorn main:app --host 0.0.0.0 --port 8000```

## Документация API

* [/api/docs](/api/docs)
