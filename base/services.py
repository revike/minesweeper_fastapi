import base64
import os
import re
import uuid
from io import BytesIO

from PIL import Image

from config.settings import MEDIA_URL


async def get_unique_name() -> str:
    """Get a unique name"""
    return f'{uuid.uuid4()}'


async def base64_to_path_save(file: base64, folder: str) -> str | None:
    """Save base64"""
    path = None
    image = file.startswith('data:image')
    if isinstance(file, str) and image:
        file_format, file_str = file.split(';base64,')
        ext = file_format.split('/')[-1]
        path = await save_image(file_str, ext, folder)
    return f'{MEDIA_URL}{path}' if path else None


async def save_image(image: str, image_format: str, folder: str) -> str:
    """Save image"""
    base64_data = re.sub('^data:image/.*;base64,', '', image)
    byte_data = base64.b64decode(base64_data)
    image_data = BytesIO(byte_data)
    img = Image.open(image_data)
    if img.mode in ('RGBA', 'P'):
        img = img.convert('RGB')
    img_name = f'{await get_unique_name()}.{image_format}'
    os.makedirs(os.path.join(MEDIA_URL, f'{folder}'), exist_ok=True)
    result_image_path = f'{folder}/{img_name}'
    image_path = f'{MEDIA_URL}{result_image_path}'
    img.save(image_path)
    return result_image_path


async def delete_old_file(old_file):
    """Delete old file"""
    if os.path.isfile(f'{old_file}'):
        os.remove(old_file)
