from datetime import datetime

from pydantic import BaseModel, field_validator

from config.settings import DATETIME_FORMAT


class BaseValidationSchema(BaseModel):
    """Base validation Error Schema"""
    error: str = 'Произошла непредвиденная ошибка'


class BaseValidateSchema(BaseModel):
    """Base Validate Schema"""
    detail: str


class BaseSchema(BaseModel):
    """Base Schema"""

    @field_validator('created_at', 'updated_at', check_fields=False)
    def custom_datetime_format(cls, value):
        return datetime.strftime(value, DATETIME_FORMAT)
