class CustomException(Exception):
    """CustomException"""

    def __init__(self, name: str):
        self.name = name
