from fastapi import Depends, HTTPException, Request
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from config.session import get_async_session
from config.settings import TOKEN_AUTH_TYPE
from users.models import User
from users.repositories import UserRepository
from users.utils import AccessToken


class JWTBearer(HTTPBearer):
    """JWT Bearer"""

    def __init__(self, auto_error: bool = True):
        super().__init__(auto_error=auto_error)

    async def __call__(self, request: Request):
        credentials: HTTPAuthorizationCredentials = await super().__call__(request)
        if credentials:
            if credentials.scheme != TOKEN_AUTH_TYPE:
                raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Invalid authentication scheme.")
            if not await self.verify_access_token(credentials.credentials):
                raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Invalid token or expired token.")
            return credentials.credentials
        else:
            raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Invalid authorization code.")

    @classmethod
    async def verify_access_token(cls, token: str) -> bool:
        """Verify access token"""
        is_token_valid: bool = False
        payload = AccessToken(token).payload
        if payload:
            is_token_valid = True
        return is_token_valid

    @classmethod
    async def current_user(cls, request: Request, session: AsyncSession = Depends(get_async_session)) -> User | None:
        """Current user"""
        credentials: HTTPAuthorizationCredentials = await super().__call__(cls, request)
        if credentials:
            token = credentials.credentials
            payload = AccessToken(token).payload
            user_id = payload['user_id']
            async with session.begin():
                user = await UserRepository(session).get_user_by_id(user_id=user_id)
                if user:
                    return user
