from sqlalchemy.ext.asyncio import AsyncSession

from base.exception_handlers import CustomException
from game.repositories import GameRepository
from game.schemas import GameCreateSchema, GameResponseSchema, GameTurnSchema


class GameSession:
    """Game Session"""

    def __init__(self, session: AsyncSession):
        self.session = session
        self.repository = GameRepository(self.session)

    async def create_game(self, body: GameCreateSchema) -> GameResponseSchema:
        """Create Game"""
        width, height, mines_count = body.width, body.height, body.mines_count
        if width < 2 or width > 30:
            raise CustomException('ширина поля должна быть не менее 2 и не более 30')
        if height < 2 or height > 30:
            raise CustomException('высота поля должна быть не менее 2 и не более 30')
        min_mines_count, max_mines_count = 1, (width * height) - 1
        if mines_count < min_mines_count or mines_count > max_mines_count:
            raise CustomException(f'количество мин должно быть не менее {min_mines_count} и не более {max_mines_count}')
        async with self.session.begin():
            game, game_field = await self.repository.create_game(width, height, mines_count)
            return GameResponseSchema(
                game_id=f'{game.game_id}',
                width=game.width,
                height=game.height,
                mines_count=game.mines_count,
                field=game_field.field,
                completed=game.completed
            )

    async def turn_game(self, body: GameTurnSchema) -> GameResponseSchema:
        """Turn game"""
        game_id, col, row = body.game_id, body.col, body.row
        game, game_field = await self.repository.turn_game(game_id, col, row)
        return GameResponseSchema(
            game_id=f'{game.game_id}',
            width=game.width,
            height=game.height,
            mines_count=game.mines_count,
            field=game_field.field,
            completed=game.completed
        )
