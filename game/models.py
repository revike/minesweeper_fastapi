import json
import uuid
from datetime import datetime

from sqlalchemy import Column, Integer, TIMESTAMP, Boolean, ForeignKey, JSON
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import declarative_base, relationship

Base = declarative_base()


class Game(Base):
    """Gema Model"""
    __tablename__ = 'game'

    game_id: uuid.UUID = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    width: int = Column(Integer, default=10)
    height: int = Column(Integer, default=10)
    mines_count: int = Column(Integer, default=10)
    completed: bool = Column(Boolean, default=False)
    created_at: datetime = Column(TIMESTAMP, default=datetime.now)
    updated_at: datetime = Column(TIMESTAMP, default=datetime.now, onupdate=datetime.now)

    game_filed = relationship('GameField', back_populates='game')


class GameField(Base):
    """Game Field Model"""
    __tablename__ = 'game_field'

    id: int = Column(Integer, primary_key=True)
    game_id = Column(UUID, ForeignKey('game.game_id'))
    field: json = Column(JSON, nullable=True)
    field_bomb: json = Column(JSON, nullable=True)
    updated_at: datetime = Column(TIMESTAMP, default=datetime.now, onupdate=datetime.now)

    game = relationship('Game', back_populates='game_filed')
