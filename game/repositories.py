import datetime
from typing import Tuple

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from base.exception_handlers import CustomException
from config.settings import TIME_LIVE_SECONDS
from game.models import Game, GameField
from game.services import create_field, create_bomb_field, get_game_data


class GameRepository:
    """Game Repository"""

    def __init__(self, db_session: AsyncSession) -> None:
        self.db_session = db_session

    async def create_game(self, width: int, height: int, mines_count: int) -> Tuple[Game, GameField]:
        """Create game"""
        new_game = Game(width=width, height=height, mines_count=mines_count)
        self.db_session.add(new_game)
        await self.db_session.flush()
        field = await create_field(width, height)
        field_bomb = await create_bomb_field(width, height, field, mines_count)
        game_field = GameField(game_id=new_game.game_id, field=field, field_bomb=field_bomb)
        self.db_session.add(game_field)
        await self.db_session.flush()
        return new_game, game_field

    async def turn_game(self, game_id, col, row) -> Tuple[Game, GameField]:
        """Turn game"""
        error_message = f'игра с идентификатором {game_id} не была создана или устарела (неактуальна)'
        datetime_now = datetime.datetime.now()
        query = select(Game, GameField).join(GameField, Game.game_id == GameField.game_id).filter_by(game_id=game_id)
        game_query = await self.db_session.execute(query)
        game = game_query.fetchone()
        if game:
            game_update = game[1].updated_at
            if (datetime_now - game_update).seconds > TIME_LIVE_SECONDS:
                raise CustomException(error_message)
            if game[0].completed:
                raise CustomException('игра завершена')
            if row < 0 or row >= game[0].height:
                raise CustomException(f'ряд должен быть неотрицательным и менее высоты {game[0].height}')
            if col < 0 or col >= game[0].width:
                raise CustomException(f'колонка должна быть неотрицательной и менее ширины {game[0].width}')
            if game[1].field[row][col] != ' ':
                raise CustomException('уже открытая ячейка')
            game, game_field = await get_game_data(self.db_session, game[0], game[1], col, row)
            return game, game_field
        raise CustomException(error_message)
