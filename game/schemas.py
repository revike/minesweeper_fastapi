import uuid
from typing import List

from pydantic import BaseModel, Field


class GameCreateSchema(BaseModel):
    """Game Create Schema"""
    width: int = Field(title='width')
    height: int = Field(title='height')
    mines_count: int = Field(title='mines_count')


class GameResponseSchema(BaseModel):
    """Game Create Response Schema"""
    game_id: uuid.UUID = Field(title='game_id')
    width: int = Field(title='width')
    height: int = Field(title='height')
    mines_count: int = Field(title='mines_count')
    field: List[List] = Field(title='field')
    completed: bool = Field(title='completed')


class GameTurnSchema(BaseModel):
    """Game Turn Schema"""
    game_id: uuid.UUID = Field(title='game_id')
    col: int = Field(title='col')
    row: int = Field(title='row')
