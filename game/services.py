import copy
import random
from typing import Tuple, List

from sqlalchemy import update
from sqlalchemy.ext.asyncio import AsyncSession

from game.models import Game, GameField


async def create_field(width: int, height: int) -> List[List]:
    """Create field"""
    return [[' ' for _ in range(width)] for _ in range(height)]


async def create_bomb_field(width: int, height: int, fields: List[List], mines_count: int) -> List[List]:
    """Create Bomb field"""
    result_fields = copy.deepcopy(fields)
    result = []

    cells_width = [i for i in range(width)]
    cells_height = [i for i in range(height)]

    for i in cells_width:
        cell_list = list(map(lambda x: [x, i], cells_height))
        for cell in cell_list:
            result.append(cell)

    indexes = random.sample([i for i in range(len(result))], mines_count)
    for idx in indexes:
        cell = result[idx]
        result_fields[cell[0]][cell[1]] = 'X'

    row, col = 0, 0
    for field in result_fields:
        for _ in field:
            if field[row] == 'X':
                row += 1 if len(field) > row + 1 else 0
                if row == 0:
                    col += 1 if len(result_fields) > col + 1 else 0
                    if col == 0:
                        break
                continue
            else:
                result_fields[col][row] = await set_field(result_fields, field, col, row)
                row += 1 if len(field) > row + 1 else 0
                if row == 0:
                    col += 1 if len(result_fields) > col + 1 else 0
                    if col == 0:
                        break
        row, col = 0, col + 1 if len(result_fields) > col + 1 else 0
    return result_fields


async def set_field(fields: List[List], field: List, col: int, row: int) -> str:
    """Set field"""
    result = 0

    async def get_result(res: int) -> int:
        """Get result"""
        if 0 <= row < len(field) and 0 <= col < len(fields):
            if fields[col][row] == 'X':
                res += 1
        return res

    row -= 1
    result = await get_result(result)
    col -= 1
    result = await get_result(result)
    row += 1
    result = await get_result(result)
    row += 1
    result = await get_result(result)
    col += 1
    result = await get_result(result)
    col += 1
    result = await get_result(result)
    row -= 1
    result = await get_result(result)
    row -= 1
    result = await get_result(result)
    return f'{result}'


async def get_game_data(db_session: AsyncSession, game: Game,
                        game_field: GameField, col: int, row: int) -> Tuple[Game, GameField]:
    """Get game data"""
    if not game.completed:
        field = copy.deepcopy(game_field.field)
        field_bomb = copy.deepcopy(game_field.field_bomb)
        obj_bomb = field_bomb[row][col]
        if obj_bomb not in ('0', 'X'):
            if obj_bomb != 'X':
                field[row][col] = obj_bomb
        elif obj_bomb == 'X':
            field = game_field.field_bomb
            game.completed = True
        elif obj_bomb == '0':
            field, cells = await set_field_zero(field, field_bomb, obj_bomb, row, col)
            for cell in cells:
                await set_field_zero(field, field_bomb, obj_bomb, cell[0], cell[1], cells)
        game_field.field = field
        game, game_field = await check_win_game(game, game_field)
        field_query = update(GameField).filter_by(game_id=game_field.game_id).values(field=game_field.field)
        await db_session.execute(field_query)
        game_query = update(Game).filter_by(game_id=game.game_id).values(completed=game.completed)
        await db_session.execute(game_query)
        await db_session.commit()
    return game, game_field


async def set_field_zero(fields: List[List], field_bomb: List[List], obj_bomb: str, row: int,
                         col: int, cells: List | None = None) -> Tuple[List[List], List]:
    """Set field zero"""
    if cells is None:
        cells = []
    fields[row][col] = obj_bomb

    async def get_cell_value(fields_list: List[List], fields_bomb: List[List],
                             row_value: int, col_value: int) -> Tuple[List[List], bool]:
        """Get cell value"""
        check_cell_zero = False
        if 0 <= row_value < len(fields_list) and 0 <= col_value < len(fields_list[0]):
            if fields[row][col] == ' ':
                cell_value = fields_bomb[row_value][col_value]
                fields_list[row_value][col_value] = cell_value
                if cell_value == '0':
                    check_cell_zero = True
        return fields_list, check_cell_zero

    row -= 1
    fields, cell_zero = await get_cell_value(fields, field_bomb, row, col)
    if cell_zero and (row, col,) not in cells:
        cells.append((row, col,))
    col -= 1
    fields, cell_zero = await get_cell_value(fields, field_bomb, row, col)
    if cell_zero and (row, col,) not in cells:
        cells.append((row, col,))
    row += 1
    fields, cell_zero = await get_cell_value(fields, field_bomb, row, col)
    if cell_zero and (row, col,) not in cells:
        cells.append((row, col,))
    row += 1
    fields, cell_zero = await get_cell_value(fields, field_bomb, row, col)
    if cell_zero and (row, col,) not in cells:
        cells.append((row, col,))
    col += 1
    fields, cell_zero = await get_cell_value(fields, field_bomb, row, col)
    if cell_zero and (row, col,) not in cells:
        cells.append((row, col,))
    col += 1
    fields, cell_zero = await get_cell_value(fields, field_bomb, row, col)
    if cell_zero and (row, col,) not in cells:
        cells.append((row, col,))
    row -= 1
    fields, cell_zero = await get_cell_value(fields, field_bomb, row, col)
    if cell_zero and (row, col,) not in cells:
        cells.append((row, col,))
    row -= 1
    fields, cell_zero = await get_cell_value(fields, field_bomb, row, col)
    if cell_zero and (row, col,) not in cells:
        cells.append((row, col,))

    return fields, cells


async def check_win_game(game: Game, game_field: GameField) -> Tuple[Game, GameField]:
    """Check Win Game"""
    mines_count = 0
    for fields in game_field.field:
        for field in fields:
            if field == ' ':
                mines_count += 1
    if mines_count == game.mines_count:
        game.completed = True
        result_win = copy.deepcopy(game_field.field_bomb)
        for i, l in enumerate(result_win):
            for k, j in enumerate(l):
                if j == 'X':
                    result_win[i][k] = 'M'
        game_field.field = result_win
        game.completed = True
    return game, game_field
