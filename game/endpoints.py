from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from config.session import get_async_session
from game.responses import game_responses
from game.schemas import GameCreateSchema, GameResponseSchema, GameTurnSchema
from game.sessions import GameSession

main_router = APIRouter()


@main_router.post('/new', responses=game_responses, description='Начало новой игры')
async def new_game(
        body: GameCreateSchema,
        session: AsyncSession = Depends(get_async_session)) -> GameResponseSchema:
    """New game endpoint"""
    return await GameSession(session).create_game(body)


@main_router.post('/turn', responses=game_responses, description='Ход пользователя')
async def turn(
        body: GameTurnSchema,
        session: AsyncSession = Depends(get_async_session)) -> GameResponseSchema:
    """Turn endpoint"""
    return await GameSession(session).turn_game(body)
