from starlette import status

from base.schemas import BaseValidationSchema
from game.schemas import GameResponseSchema

game_responses = {
    status.HTTP_200_OK: {'model': GameResponseSchema},
    status.HTTP_400_BAD_REQUEST: {'model': BaseValidationSchema},
}
