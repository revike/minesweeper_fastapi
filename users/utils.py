import abc
import datetime
from typing import Tuple

import jwt
import pytz
from passlib.context import CryptContext

from config.settings import ACCESS_TOKEN_EXPIRE, SECRET_KEY, TIME_ZONE, REFRESH_TOKEN_EXPIRE, ALGORITHM
from users.models import User


class HashingPassword:
    """Hashing password"""
    pwd_context = CryptContext(schemes=['bcrypt'], deprecated='auto')

    @classmethod
    def verify_password(cls, password: str, hashed_password: str) -> bool:
        """Verify password"""
        return cls.pwd_context.verify(password, hashed_password)

    @classmethod
    def get_password_hash(cls, password: str) -> str:
        """Get password hash"""
        return cls.pwd_context.hash(password)


class Token(abc.ABC):
    """Token"""
    token_type: str = 'access'
    expire_token: datetime.timedelta = datetime.timedelta(minutes=60)
    key: str = 'key'
    algorithm = 'HS256'

    def __init__(self, token: str = None):
        self.jwt_token = token

    @abc.abstractmethod
    def for_user(self, user: User, sub: str = None, expires_delta: datetime.timedelta = None) -> str:
        dt_now = datetime.datetime.now().astimezone(pytz.timezone(TIME_ZONE))
        if expires_delta:
            expire = dt_now + expires_delta
        else:
            expire = dt_now + self.expire_token
        payload = {
            'token_type': self.token_type,
            'exp': expire,
            'user_id': user.id,
        }
        if sub:
            payload['sub'] = sub
        token = jwt.encode(payload=payload, key=self.key, algorithm=self.algorithm)
        return token

    @property
    def token(self) -> str | None:
        """Get token"""
        return f'{self.jwt_token}'

    @abc.abstractmethod
    def payload(self) -> dict | None:
        """Get payload"""
        try:
            payload = jwt.decode(self.jwt_token, key=self.key, algorithms=self.algorithm)
            return payload
        except (jwt.exceptions.InvalidSignatureError, jwt.exceptions.DecodeError, jwt.exceptions.ExpiredSignatureError):
            return None


class AccessToken(Token):
    """Access Token"""
    expire_token = ACCESS_TOKEN_EXPIRE
    key = SECRET_KEY
    algorithm = ALGORITHM

    @classmethod
    def for_user(cls, user: User, sub: str = None, expires_delta: datetime.timedelta = None) -> str:
        """Get token for user"""
        return super().for_user(self=cls, user=user, sub=sub, expires_delta=expires_delta)

    @property
    def payload(self) -> dict | None:
        """Get payload"""
        return super().payload()


class RefreshToken(Token):
    """Refresh Token"""
    token_type = 'refresh'
    expire_token = REFRESH_TOKEN_EXPIRE
    key = SECRET_KEY
    algorithm = ALGORITHM

    @classmethod
    def for_user(cls, user: User, sub: str = None, expires_delta: datetime.timedelta = None) -> str:
        """Get token for user"""
        return super().for_user(self=cls, user=user, sub=sub, expires_delta=expires_delta)

    @property
    def payload(self) -> dict | None:
        """Get payload"""
        return super().payload()

    def update_tokens(self, payload: dict) -> Tuple[str, str]:
        """Update tokens"""
        dt_now = datetime.datetime.now().astimezone(pytz.timezone(TIME_ZONE))
        expire = dt_now + self.expire_token
        payload['exp'] = expire
        refresh_token = jwt.encode(payload=payload, key=self.key, algorithm=self.algorithm)
        expire_access = dt_now + ACCESS_TOKEN_EXPIRE
        payload['exp'] = expire_access
        access_token = jwt.encode(payload=payload, key=self.key, algorithm=self.algorithm)
        return access_token, refresh_token
