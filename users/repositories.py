from typing import Tuple

from fastapi import HTTPException
from sqlalchemy import select, update
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from base.services import base64_to_path_save, delete_old_file
from users.models import User, Profile


class UserRepository:
    """User Repository"""

    def __init__(self, db_session: AsyncSession = None) -> None:
        self.db_session = db_session

    async def create_user(self, email: str, password: str) -> User:
        """Create user"""
        user = User(email=email, password=password)
        self.db_session.add(user)
        try:
            await self.db_session.flush()
            profile = Profile(user_id=user.id)
            self.db_session.add(profile)
            await self.db_session.flush()
            return user
        except IntegrityError:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='user already exists')

    async def get_user_by_email(self, email: str) -> User | None:
        """Get user by email"""
        query = select(User).filter_by(email=email)
        res = await self.db_session.execute(query)
        user = res.first()
        res, = user or (None,)
        return res

    async def get_user_by_id(self, user_id: int) -> User | None:
        """Get user by email"""
        query = select(User).filter_by(id=user_id)
        res = await self.db_session.execute(query)
        user = res.first()
        res, = user or (None,)
        return res

    async def get_profile_by_user(self, user: User) -> Profile | None:
        """Get profile by user"""
        query = select(Profile).filter_by(user_id=user.id)
        res = await self.db_session.execute(query)
        profile = res.first()
        res, = profile or (None,)
        return res

    async def update_profile_by_user(self, user: User, **params: dict) -> Tuple[User, Profile | None]:
        """Update User and Profile by user"""
        query_profile = select(Profile).filter_by(user_id=user.id)
        res = await self.db_session.execute(query_profile)
        res_first = res.first()
        profile, = res_first or (None,)
        if 'profile' in params:
            if not profile:
                profile = Profile(user_id=user.id)
                self.db_session.add(profile)
                await self.db_session.flush()
            profile_params: dict = params.pop('profile')
            await delete_old_file(profile.photo)
            photo = profile_params['photo'] if 'photo' in profile_params else None
            if photo:
                photo_path = await base64_to_path_save(profile_params['photo'], 'allow_all')
                profile_params['photo'] = photo_path
            query_profile_update = update(Profile).filter_by(user_id=user.id).values(profile_params)
            await self.db_session.execute(query_profile_update)
        query_user_update = update(User).filter_by(id=user.id).values(params)
        await self.db_session.execute(query_user_update)
        await self.db_session.commit()
        return user, profile
