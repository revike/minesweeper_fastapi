from datetime import datetime

from pydantic import Field, EmailStr

from base.schemas import BaseSchema


class RegisterUserSchema(BaseSchema):
    """Register User Schema"""
    email: EmailStr = Field(title='email')
    password: str = Field(title='password')


class RegisterResponseSchema(BaseSchema):
    """Register Response Schema"""
    id: int = Field(title='id')
    email: EmailStr = Field(title='email')
    created_at: datetime = Field(title='created_at')
    updated_at: datetime = Field(title='updated_at')


class LoginUserSchema(BaseSchema):
    """Register User Schema"""
    email: str = Field(title='email')
    password: str = Field(title='password')


class AccessTokenSchema(BaseSchema):
    """AccessToken Schema"""
    access: str = Field(title='access')


class AccessRefreshTokenSchema(AccessTokenSchema):
    """Access and Refresh Tokens Schema"""
    refresh: str = Field(title='refresh')


class ProfileResponseSchema(BaseSchema):
    """Profile Response Schema"""
    id: int = Field(title='id')
    photo: str | None = Field(title='photo')
    created_at: datetime = Field(title='created_at')
    updated_at: datetime = Field(title='updated_at')


class UserResponseSchema(BaseSchema):
    """User Response Schema"""
    id: int = Field(title='id')
    email: EmailStr = Field(title='email')
    first_name: str | None = Field(title='first_name')
    last_name: str | None = Field(title='last_name')
    profile: ProfileResponseSchema | None = Field(title='profile')
    created_at: datetime = Field(title='created_at')
    updated_at: datetime = Field(title='updated_at')


class ProfileUpdateSchema(BaseSchema):
    """ProfileUpdateSchema"""
    photo: str | None = Field(title='photo', default=None)


class UserUpdateSchema(BaseSchema):
    """User Update Schema"""
    email: EmailStr | None = Field(title='email', default=None)
    first_name: str | None = Field(title='first_name', default=None)
    last_name: str | None = Field(title='last_name', default=None)
    profile: ProfileUpdateSchema | None = Field(title='profile', default=None)


class ProfileUpdateResponseSchema(BaseSchema):
    """Profile Update Response Schema"""
    id: int = Field(title='id')
    photo: str | None = Field(title='photo')


class UserUpdateResponseSchema(BaseSchema):
    """User Update Response Schema"""
    id: int = Field(title='id')
    email: EmailStr = Field(title='email')
    first_name: str | None = Field(title='first_name')
    last_name: str | None = Field(title='last_name')
    profile: ProfileUpdateResponseSchema | None = Field(title='profile')
    created_at: datetime = Field(title='created_at')
    updated_at: datetime = Field(title='updated_at')
