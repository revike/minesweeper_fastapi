from starlette import status

from base.schemas import BaseValidateSchema
from users.schemas import RegisterResponseSchema, AccessTokenSchema, UserResponseSchema, UserUpdateResponseSchema

register_responses = {
    status.HTTP_201_CREATED: {'model': RegisterResponseSchema},
    status.HTTP_400_BAD_REQUEST: {'model': BaseValidateSchema},
}

login_responses = {
    status.HTTP_200_OK: {'model': AccessTokenSchema},
    status.HTTP_401_UNAUTHORIZED: {'model': BaseValidateSchema},
}

profile_responses = {
    status.HTTP_200_OK: {'model': UserResponseSchema},
    status.HTTP_403_FORBIDDEN: {'model': BaseValidateSchema},
}

user_update_responses = {
    status.HTTP_200_OK: {'model': UserUpdateResponseSchema},
    status.HTTP_400_BAD_REQUEST: {'model': BaseValidateSchema},
    status.HTTP_403_FORBIDDEN: {'model': BaseValidateSchema},
}
