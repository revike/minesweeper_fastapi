from fastapi import APIRouter, Depends, Response, Request
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from base.permission import JWTBearer
from config.session import get_async_session
from users.models import User
from users.responses import register_responses, login_responses, profile_responses, user_update_responses
from users.schemas import RegisterUserSchema, RegisterResponseSchema, LoginUserSchema, AccessTokenSchema, \
    UserResponseSchema, UserUpdateResponseSchema, UserUpdateSchema
from users.services import set_cookies
from users.sessions import UserSession

user_router = APIRouter()


@user_router.post(
    '/register', responses=register_responses, description='Регистрация', status_code=status.HTTP_201_CREATED)
async def register(
        body: RegisterUserSchema,
        session: AsyncSession = Depends(get_async_session)) -> RegisterResponseSchema:
    """Register User"""
    return await UserSession(session).create_user(body)


@user_router.post('/login', responses=login_responses, description='Авторизация')
async def login(
        response: Response,
        body: LoginUserSchema,
        session: AsyncSession = Depends(get_async_session)) -> AccessTokenSchema:
    """Login"""
    user = await UserSession(session).login_user(body)
    set_cookies(response=response, key='refresh', value=user.refresh)
    return user


@user_router.post('/refresh', responses=login_responses, description='Refresh Token')
async def refresh(request: Request, response: Response) -> AccessTokenSchema:
    """Refresh Token"""
    refresh_token = request.cookies.get('refresh')
    token = await UserSession.check_refresh_token(refresh_token)
    set_cookies(response=response, key='refresh', value=token.refresh)
    return token


@user_router.get('/profile', responses=profile_responses, description='Профиль',
                 response_model=UserResponseSchema, dependencies=[Depends(JWTBearer())])
async def profile(user: User = Depends(JWTBearer().current_user),
                  session: AsyncSession = Depends(get_async_session)) -> UserResponseSchema:
    """Profile"""
    user_profile = await UserSession(session).get_profile_by_user(user)
    return user_profile


@user_router.patch('/update', responses=user_update_responses, description='Редактирование профиля',
                   dependencies=[Depends((JWTBearer()))])
async def update(body: UserUpdateSchema, user: User = Depends(JWTBearer().current_user),
                 session: AsyncSession = Depends(get_async_session)) -> UserUpdateResponseSchema:
    """Update User"""
    updated_user_params = body.dict(exclude_none=True)
    user_profile = await UserSession(session).update_profile_by_user(user, updated_user_params)
    return user_profile
