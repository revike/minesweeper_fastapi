from datetime import datetime

from sqlalchemy import Column, TIMESTAMP, Integer, String, ForeignKey
from sqlalchemy.orm import declarative_base, relationship

Base = declarative_base()


class User(Base):
    """User Model"""
    __tablename__ = 'user'

    id: int = Column(Integer, primary_key=True)
    email: str = Column(String(256), unique=True, index=True, nullable=False)
    password: str = Column(String(256), unique=True, nullable=False)
    first_name: str = Column(String(128), nullable=True)
    last_name: str = Column(String(128), nullable=True)
    created_at: datetime = Column(TIMESTAMP, default=datetime.now)
    updated_at: datetime = Column(TIMESTAMP, default=datetime.now, onupdate=datetime.now)

    profile = relationship('Profile', back_populates='user', uselist=False)


class Profile(Base):
    """Profile Model"""
    __tablename__ = 'profile'
    id: int = Column(Integer, primary_key=True)
    user_id: int = Column(Integer, ForeignKey('user.id'))
    photo: str = Column(String, nullable=True)
    created_at: datetime = Column(TIMESTAMP, default=datetime.now)
    updated_at: datetime = Column(TIMESTAMP, default=datetime.now, onupdate=datetime.now)

    user = relationship('User', back_populates='profile', uselist=False)
