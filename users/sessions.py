from fastapi import HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from users.models import User
from users.repositories import UserRepository
from users.schemas import RegisterResponseSchema, RegisterUserSchema, LoginUserSchema, AccessRefreshTokenSchema, \
    UserResponseSchema, ProfileResponseSchema, UserUpdateResponseSchema, ProfileUpdateResponseSchema
from users.utils import HashingPassword, AccessToken, RefreshToken


class UserSession:
    """User session"""

    def __init__(self, session: AsyncSession):
        self.session = session
        self.repository = UserRepository(self.session)

    async def create_user(self, body: RegisterUserSchema) -> RegisterResponseSchema:
        """Create User"""
        email, password = body.email, body.password
        async with self.session.begin():
            hashed_password = HashingPassword.get_password_hash(password=password)
            user = await self.repository.create_user(email=email, password=hashed_password)
            return RegisterResponseSchema(
                id=user.id,
                email=user.email,
                created_at=user.created_at,
                updated_at=user.updated_at,
            )

    async def login_user(self, body: LoginUserSchema) -> AccessRefreshTokenSchema:
        """login user"""
        email, password = body.email, body.password
        async with self.session.begin():
            user = await self.repository.get_user_by_email(email)
            if user:
                check_password = HashingPassword.verify_password(password, user.password)
                if check_password:
                    access = AccessToken.for_user(user)
                    refresh = RefreshToken.for_user(user)
                    return AccessRefreshTokenSchema(access=access, refresh=refresh)
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail='incorrect email or password')

    @classmethod
    async def check_refresh_token(cls, refresh_t: str) -> AccessRefreshTokenSchema:
        refresh_token = RefreshToken(refresh_t)
        payload = refresh_token.payload
        if isinstance(payload, dict):
            access, refresh = refresh_token.update_tokens(payload)
            return AccessRefreshTokenSchema(access=access, refresh=refresh)
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail='token is invalid')

    async def get_profile_by_user(self, user: User) -> UserResponseSchema:
        """Get profile"""
        async with self.session.begin():
            profile = await self.repository.get_profile_by_user(user)
            return UserResponseSchema(
                id=user.id,
                email=user.email,
                first_name=user.first_name,
                last_name=user.last_name,
                profile=ProfileResponseSchema(
                    id=profile.id,
                    photo=profile.photo,
                    created_at=profile.created_at,
                    updated_at=profile.updated_at,
                ) if profile else None,
                created_at=user.created_at,
                updated_at=user.updated_at
            )

    async def update_profile_by_user(self, user: User, params: dict) -> UserUpdateResponseSchema:
        """Update Profile by user"""
        async with self.session.begin():
            user, profile = await self.repository.update_profile_by_user(user, **params)
            return UserUpdateResponseSchema(
                id=user.id,
                email=user.email,
                first_name=user.first_name,
                last_name=user.last_name,
                profile=ProfileUpdateResponseSchema(
                    id=profile.id,
                    photo=profile.photo,
                ) if profile else None,
                created_at=user.created_at,
                updated_at=user.updated_at,
            )
