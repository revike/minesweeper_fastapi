import datetime
from typing import Literal

from fastapi import Response
from config.settings import REFRESH_TOKEN_EXPIRE, DOMAIN, SECURE, SAME_SITE


def set_cookies(response: Response, key: str, value: str, expires: datetime.datetime = None,
                domain: str = DOMAIN, secure: bool = SECURE, httponly: bool = True,
                same_site: Literal['lax', 'strict', 'none'] | None = SAME_SITE) -> Response:
    """Set cookies"""
    if expires is None:
        dt_now = datetime.datetime.now().astimezone(datetime.timezone.utc)
        expires = dt_now + REFRESH_TOKEN_EXPIRE
    response.set_cookie(key=key, value=value, expires=expires,
                        domain=domain, secure=secure, httponly=httponly, samesite=same_site)
    return response
