from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from users.models import Profile


class FileAllowAllRepository:
    """File Allow All Repository"""

    def __init__(self, session: AsyncSession) -> None:
        self.db_session = session

    async def get_file(self, fine_name: str) -> str | None:
        """Get file"""
        query = select(Profile).filter_by(photo=f'media/allow_all/{fine_name}')
        res = await self.db_session.execute(query)
        file_first = res.first()
        file, = file_first or (None,)
        return file.photo if file else None
