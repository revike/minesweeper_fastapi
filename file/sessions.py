from sqlalchemy.ext.asyncio import AsyncSession

from file.repositories import FileAllowAllRepository


class FileAllowAllSession:
    """File Allow All Session"""

    def __init__(self, session: AsyncSession):
        self.session = session
        self.repository = FileAllowAllRepository(self.session)

    async def get_file_bytes(self, fine_name: str) -> bytes | None:
        """Get file bytes"""
        async with self.session.begin():
            file = await self.repository.get_file(fine_name)
            if file:
                try:
                    with open(f'{file}', 'rb') as f:
                        file = f.read()
                except FileNotFoundError:
                    file = None
            return file
