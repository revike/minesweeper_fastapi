from fastapi import APIRouter, Depends, HTTPException
from starlette import status
from starlette.responses import Response

from config.session import get_async_session
from file.sessions import FileAllowAllSession

file_router = APIRouter()


@file_router.get('/allow_all/{file_name}', status_code=status.HTTP_200_OK, include_in_schema=False)
async def get_file_allow_all(file_name: str, session=Depends(get_async_session)):
    """Get file allow all"""
    file_bytes = await FileAllowAllSession(session).get_file_bytes(file_name)
    if file_bytes:
        return Response(file_bytes)
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='File not found')
