import datetime
import os
from pathlib import Path

from dotenv import load_dotenv

BASE_DIR = Path(__file__).resolve().parent.parent

dot_env = os.path.join(BASE_DIR, '.env')
load_dotenv(dotenv_path=dot_env)

ENV_TYPE = os.environ.get('ENV_TYPE')
DEBUG = (os.getenv('DEBUG', 'False') == 'True')

SECRET_KEY = os.getenv('SECRET_KEY')
BACK_URL = os.getenv('BACK_URL')

# Database
POSTGRES_DB = os.environ.get('POSTGRES_DB')
POSTGRES_USER = os.environ.get('POSTGRES_USER')
POSTGRES_PASSWORD = os.environ.get('POSTGRES_PASSWORD')
POSTGRES_HOST = os.environ.get('POSTGRES_HOST')
POSTGRES_PORT = os.environ.get('POSTGRES_PORT')

"""
alembic init migrations
alembic revision --autogenerate -m 'database create'
alembic upgrade head
"""

if DEBUG or ENV_TYPE in ('test', 'local',):
    DATABASE_URL = 'sqlite+aiosqlite:///./db.sqlite3'
else:
    DATABASE_URL = f'postgresql+asyncpg:' \
                   f'//{POSTGRES_USER}:{POSTGRES_PASSWORD}@' \
                   f'{POSTGRES_HOST}:{POSTGRES_PORT}/{POSTGRES_DB}'

# Media
MEDIA_URL = 'media/'

# Datetime settings
DATE_FORMAT = '%Y-%m-%d'
TIME_FORMAT = '%H:%M'
SECOND_FORMAT = ':%S'
DATETIME_FORMAT = f'{DATE_FORMAT} {TIME_FORMAT}{SECOND_FORMAT}'

TIME_ZONE = 'UTC'
TIME_LIVE_SECONDS = 60
CORS_ORIGIN = os.environ.get('CORS_ORIGIN').split()

# Token
ACCESS_TOKEN_EXPIRE = datetime.timedelta(minutes=60)
REFRESH_TOKEN_EXPIRE = datetime.timedelta(days=7)

# Cookie
DOMAIN = os.getenv('DOMAIN')
SECURE = os.getenv('SECURE') == 'True'
SAME_SITE = os.getenv('SAME_SITE')
ALGORITHM = 'HS256'
TOKEN_AUTH_TYPE = 'Bearer'
